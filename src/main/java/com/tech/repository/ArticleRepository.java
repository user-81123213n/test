package com.tech.repository;

import com.tech.model.Article;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface ArticleRepository extends PagingAndSortingRepository<Article, Long> {

	List<Article> findTop10ByTitleContainingIgnoreCase(String title);

}

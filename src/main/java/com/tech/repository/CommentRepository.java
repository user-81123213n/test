package com.tech.repository;

import com.tech.model.Comment;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface CommentRepository extends PagingAndSortingRepository<Comment, Long> {

    @Override
    List<Comment> findAll();

    List<Comment> findByArticleIdOrderByDate(Long articleId);
}

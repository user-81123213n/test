package com.tech.service;

import com.tech.model.Article;
import java.util.List;

public interface ArticleService {

    /*
     * Save the default article.
     */
    Article save(Article article);

    /*
     * FindById will find the specific user form list.
     *
     */
    Article findById(Long id);

    /*
     * Delete a particular article with id
     */
    void delete(Long id);

    /*
     * Search Articles Table matching the title and return result with pagination.
     */
    List<Article> search(String title);

}

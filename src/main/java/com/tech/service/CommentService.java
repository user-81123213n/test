package com.tech.service;

import com.tech.model.Comment;
import java.util.List;

public interface CommentService {

    /*
     * Returns all the Comments related to article along with Pagination
     * information.
     */
    List<Comment> findAll(Long articleId);

    /*
     * Save the default article.
     */
    Comment save(Comment comment);

}

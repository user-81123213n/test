package com.tech.service;

import com.tech.model.Comment;
import com.tech.repository.CommentRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentRepository commentRepository;

    /*
     * Returns all the Comments related to article along with Pagination
     * information.
     */
    public List<Comment> findAll(Long articleId) {
        return commentRepository.findAll();
    }

    /*
     * Save the default article.
     */
    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    }

}

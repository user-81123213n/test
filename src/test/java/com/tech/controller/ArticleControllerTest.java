package com.tech.controller;

import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tech.model.Article;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ArticleControllerTest extends ControllerTest {

    public static final String NEW_TITLE = "newTitle";
    public static final String NEW_CONTENT = "newContent";

    @Before
    public void setup() throws Exception {
    }

    @Test
    public void testArticleShouldBeCreated() throws Exception {
        HttpEntity<Object> article = getHttpEntity("{\"email\": \"user1@gmail.com\", \"title\": \"hello\" }");
        ResponseEntity<Article> resultAsset = template.postForEntity("/articles", article, Article.class);
        Assert.assertNotNull(resultAsset.getBody().getId());
    }

    @Test
    public void testUpdateArticleTitle() throws Exception {
        ResponseEntity<Article> resultAsset = template.getForEntity("/articles/1", Article.class);
        resultAsset.getBody().setTitle(NEW_TITLE);
        resultAsset.getBody().setContent(NEW_CONTENT);

        HttpEntity<Object> article = getHttpEntity(resultAsset.getBody());
        template.put("/articles/1", article);

        ResponseEntity<Article> resultAssetUpdated = template.getForEntity("/articles/1", Article.class);
        Assert.assertEquals(resultAsset, resultAssetUpdated);
    }

    @Test
    public void testArticleShouldBeDeleted() throws Exception {
        ResponseEntity<Article> resultAsset = template.getForEntity("/articles/3", Article.class);
        HttpEntity<Object> article = getHttpEntity(resultAsset.getBody());
        template.delete("/articles/3", article);
        ResponseEntity<Article> resultAssetDelete = template.getForEntity("/articles/3", Article.class);
        Assert.assertEquals(resultAssetDelete.getStatusCodeValue(), 404);
    }

    @Test
    public void testGetArticleByIdShouldReturnCorrectArticle() throws Exception {
        ResponseEntity<Article> resultAsset = template.getForEntity("/articles/1", Article.class);
        Assert.assertNotNull(resultAsset.getBody().getId());
        Assert.assertEquals(resultAsset.getBody().getId().toString(), new String("1"));

    }

    @Test
    public void testSearchShouldReturnTheCorrectArticle() throws Exception {
        String url = "/articles/search?text=hello";
        ResponseEntity<List> resultAsset = template.getForEntity(url, List.class);
        Assert.assertNotNull(resultAsset.getBody());
        System.out.println();
    }


}

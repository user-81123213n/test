package com.tech.controller;

import com.tech.model.Comment;
import com.tech.repository.CommentRepository;
import com.tech.service.ArticleService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CommentControllerTest extends ControllerTest{

    private static final String EMAIL = "me@gmail.com";
    private static final String MESSAGE = "msg";

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private ArticleService articleService;

    @Test
    public void testCommentShouldBeCreated() {
        HttpEntity<Comment> comment = new HttpEntity<>(new Comment());
        comment.getBody().setArticle(articleService.findById(1L));
        comment.getBody().setEmail(EMAIL);
        comment.getBody().setMessage(MESSAGE);

        ResponseEntity<Comment> resultAsset= template.postForEntity("/articles/1/comments",comment , Comment.class);
        Assert.assertNotNull(resultAsset.getBody().getId());
    }

    @Test
    public void testGetAllCommentsDoesntreturnNull() {
        ResponseEntity<Comment[]> resultAsset= template.getForEntity("/articles/1/comments", Comment[].class);
        Assert.assertEquals(resultAsset.getStatusCodeValue(), 200);
        Assert.assertNotNull(resultAsset.getBody());
    }
}

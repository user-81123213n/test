package com.tech.model;

import com.tech.model.Article;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class ArticleTest {
    private Article articleA, articleB;

    @Test
    public void testIDuniquelyIdentifyArticle() throws Exception{
        articleA = new Article();
        articleB = new Article();

        articleA.setId(1L);
        articleA.setEmail("email@gamil.com");
        articleA.setContent("content");

        articleB.setId(2L);
        articleB.setEmail("email@gamil.com");
        articleB.setContent("content");

        Assert.assertFalse(articleA.hashCode() == articleB.hashCode());
        Assert.assertFalse(articleA.equals(articleB));

    }


}

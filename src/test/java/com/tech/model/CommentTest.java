package com.tech.model;

import com.tech.model.Article;
import com.tech.model.Comment;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CommentTest {

    private static final String MSG1 = "msg1";
    private static final String MSG2 = "msg2";

    private Comment commentA, commentB;
    private Article article;

    @Test
    public void testIDuniquelyIdentifyComment() throws Exception{
        article = new Article();
        commentA = new Comment();
        commentB = new Comment();

        commentA.setMessage(MSG1);
        commentA.setArticle(article);
        commentA.setId(1L);

        commentB.setMessage(MSG2);
        commentB.setArticle(article);
        commentB.setId(1L);

        Assert.assertFalse(commentA.hashCode() == commentB.hashCode());
        Assert.assertFalse(commentA.equals(commentB));
    }

}
